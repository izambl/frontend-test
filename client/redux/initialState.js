import { initialState as userInitialState } from './reducers/userReducer';
import { initialState as routesInitialState } from './reducers/routeReducer';

export default {
  user: userInitialState,
  routes: routesInitialState,
};
