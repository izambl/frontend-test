import { getRoutes, addRoute, deleteRoute } from '../../services/busolinea';
import { GET_ROUTES, ADD_ROUTE, DELETE_ROUTE } from './routeActionTypes';

const fetchUser = () =>
  dispatch =>
    UserServices.fetchUser()
      .then(response => dispatch({ type: FETCH_USER_SUCCESS, payload: response }))
      .catch(response => dispatch({ type: FETCH_USER_ERROR, payload: response }));
