import * as services from '../../services/busolinea';
import {
  GET_ROUTES_REQUEST, GET_ROUTES_SUCCESS, GET_ROUTES_FAILURE,
  ADD_ROUTE_REQUEST, ADD_ROUTE_SUCCESS, ADD_ROUTE_FAILURE,
  DELETE_ROUTE_REQUEST, DELETE_ROUTE_SUCCESS, DELETE_ROUTE_FAILURE,
} from './routeActionTypes';

export function loadRoutes() {
  return (dispatch) => {
    dispatch({ type: GET_ROUTES_REQUEST });

    services.getRoutes()
      .then((response) => {
        dispatch({ type: GET_ROUTES_SUCCESS, payload: response.data.data.routes });
      })
      .catch(() => dispatch({ type: GET_ROUTES_FAILURE }));
  };
}

export function addRoute(route) {
  return (dispatch) => {
    dispatch({ type: ADD_ROUTE_REQUEST });

    services.addRoute(route)
      .then(() => {
        dispatch({ type: ADD_ROUTE_SUCCESS });
        loadRoutes()(dispatch);
      })
      .catch(() => dispatch({ type: ADD_ROUTE_FAILURE }));
  };
}

export function deleteRoute(route) {
  return (dispatch) => {
    dispatch({ type: DELETE_ROUTE_REQUEST });

    services.deleteRoute(route)
      .then(() => {
        dispatch({ type: DELETE_ROUTE_SUCCESS });
        loadRoutes()(dispatch);
      })
      .catch(() => dispatch({ type: DELETE_ROUTE_FAILURE }));
  };
}
