import { combineReducers } from 'redux';

import routes from './routeReducer';
import user from './userReducer';

export default combineReducers({
  user,
  routes,
});
