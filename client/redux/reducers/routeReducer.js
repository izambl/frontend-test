import {
  GET_ROUTES_REQUEST, GET_ROUTES_SUCCESS, GET_ROUTES_FAILURE,
  DELETE_ROUTE_FAILURE,
} from '../actions/routeActionTypes';

export const initialState = {
  routes: [],
  loading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_ROUTES_REQUEST:
      return { ...state, loading: true, routes: [] };
    case GET_ROUTES_SUCCESS:
      return { ...state, loading: false, routes: action.payload };
    case GET_ROUTES_FAILURE:
      return { ...state, loading: false };
    case DELETE_ROUTE_FAILURE:
      return state;
    default: return { ...state };
  }
};
