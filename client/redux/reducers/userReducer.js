// API key should be retrieved from another service once the user is authenticated
export const initialState = {
  apiKey: process.env.API_KEY,
};

export default (state = initialState) => ({ ...state });
