import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import BusolineaApp from './components/App';

import store from './redux/store';

import 'normalize.css';
import './styles/app.scss';

ReactDOM.render(
  <Provider store={store}>
    <BusolineaApp />
  </Provider>,
  document.getElementById('busolinea-app'),
);
