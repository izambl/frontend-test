import axios from 'axios';
import store from '../redux/store';

const defaultOptions = {
  baseURL: process.env.API_HOST,
  headers: {
    'x-api-key': store.getState().user.apiKey,
  },
};

const authAxios = axios.create(defaultOptions);

export default authAxios;
