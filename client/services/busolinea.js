import client from './authClient';

// Routes
export const getRoutes = () => client.get('/test/routes');
export const addRoute = params => client.post('/test/routes', params);
export const deleteRoute = id => client.delete(`/test/routes/${id}`);

// Stations
export const getStationsOrigins = () => client.get('/test/stations/origins');
export const getStationsOrigin = id => client.get(`/test/stations/origins/${id}`);
export const getStationsOriginDestinations = id => client.get(`/test/stations/origins/${id}/destinations`);
