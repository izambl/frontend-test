import React from 'react';
import { mount } from 'enzyme';

import Route from './index';

const defaultRoute = {
  id: 'id',
  origin_id: 'city01',
  destination_id: 'cit02',
  departure_time: '2019-09-20',
  arrival_time: '2019-12-32',
  company_name: 'demo',
};
const deleteRouteSpy = jest.fn();

function getRoute(deleteRoute = deleteRouteSpy, route = defaultRoute) {
  return mount(<Route route={route} deleteRoute={deleteRoute} />);
}

/* eslint-env jest */
describe('Route', () => {
  test('it renders route', () => {
    const footerComponent = getRoute();

    expect(footerComponent.find('article.route').length).toBe(1);
    expect(footerComponent.find('article header').length).toBe(1);
    expect(footerComponent.find('article aside').length).toBe(1);
  });

  test('it executes delete on click', () => {
    const footerComponent = getRoute();
    deleteRouteSpy.mockReset();

    footerComponent.find('article aside button').simulate('click');

    expect(deleteRouteSpy).toHaveBeenCalledWith(defaultRoute.id);
  });
});
