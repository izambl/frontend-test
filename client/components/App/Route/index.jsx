import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './index.scss';

class Route extends Component {
  constructor(props) {
    super(props);

    this.state = { details: false };
    this.toggleDetails = this.toggleDetails.bind(this);
    this.renderAside = this.renderAside.bind(this);
    this.deleteRoute = this.deleteRoute.bind(this);
  }

  toggleDetails() {
    this.setState(state => ({ details: !state.details }));
  }

  deleteRoute() {
    const { deleteRoute, route } = this.props;

    deleteRoute(route.id);
  }

  renderAside() {
    const { route } = this.props;

    return (
      <aside>
        <div>{`Compañia: ${route.company_name}`}</div>
        <div>{`Hora de salida: ${route.departure_time}`}</div>
        <div>{`Hora de llegada: ${route.arrival_time}`}</div>
        <button type="button" onClick={this.deleteRoute}>eliminar</button>
      </aside>
    );
  }

  render() {
    const { route } = this.props;
    const { details } = this.state;
    const classes = classNames({
      route: true,
      'route--details': details,
    });

    return (
      <article className={classes}>
        <header>
          <button type="button" onClick={this.toggleDetails}>➤</button>
          <div>
            {`De ${route.origin_id} a ${route.destination_id}`}
          </div>
        </header>
        {this.renderAside()}
      </article>
    );
  }
}

Route.propTypes = {
  route: PropTypes.shape({
    id: PropTypes.string,
    origin_id: PropTypes.string,
    destination_id: PropTypes.string,
    departure_time: PropTypes.string,
    arrival_time: PropTypes.string,
    company_name: PropTypes.string,
  }).isRequired,
  deleteRoute: PropTypes.func.isRequired,
};

export default Route;
