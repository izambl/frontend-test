import React from 'react';

import { Header, Main, Footer } from './layout';

function BusolineaApp() {
  return (
    <>
      <Header />
      <Main />
      <Footer />
    </>
  );
}

export default BusolineaApp;
