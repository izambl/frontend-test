import React from 'react';

import './index.scss';

function Header() {
  return (
    <header id="header" className="row">
      <div className="container">
        Busolinea demo app
      </div>
    </header>
  );
}

export default Header;
