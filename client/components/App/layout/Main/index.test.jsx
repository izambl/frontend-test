import React from 'react';
import { shallow } from 'enzyme';

import Main from './index';
import Routes from '../../Routes';
import NewRoute from '../../NewRoute';

/* eslint-env jest */
describe('Main', () => {
  test('it renders main section', () => {
    const footerComponent = shallow(<Main />);

    expect(footerComponent.find('main').length).toBe(1);
    expect(footerComponent.find(Routes).length).toBe(1);
    expect(footerComponent.find(NewRoute).length).toBe(1);
  });
});
