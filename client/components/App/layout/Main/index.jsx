import React from 'react';

import Routes from '../../Routes';
import NewRoute from '../../NewRoute';

import './index.scss';

function Main() {
  return (
    <main className="row container">
      <Routes />
      <NewRoute />
    </main>
  );
}

export default Main;
