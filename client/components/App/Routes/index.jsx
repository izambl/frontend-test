import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Route from '../Route';
import { loadRoutes, deleteRoute } from '../../../redux/actions/routeActions';
import './index.scss';

class Routes extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.reloadRoutes = this.reloadRoutes.bind(this);
  }

  componentDidMount() {
    this.reloadRoutes();
  }

  reloadRoutes() {
    const { loadRoutesAction } = this.props;

    loadRoutesAction();
  }

  render() {
    const { routes, deleteRouteAction, loading } = this.props;
    const classNames = classnames({
      'routes-list': true,
      loading,
    });

    return (
      <section className={classNames}>
        <header>
          <h4>
            Rutas activas
            <button type="button" onClick={this.reloadRoutes}>↻</button>
          </h4>
        </header>

        { routes.map(route => (
          <Route key={route.id} route={route} deleteRoute={deleteRouteAction} />
        ))
        }
      </section>
    );
  }
}

Routes.propTypes = {
  routes: PropTypes.arrayOf(PropTypes.object).isRequired,
  loadRoutesAction: PropTypes.func.isRequired,
  deleteRouteAction: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
};

export const mapStateToProps = state => ({
  routes: state.routes.routes,
  loading: state.routes.loading,
});

export default connect(mapStateToProps, {
  loadRoutesAction: loadRoutes,
  deleteRouteAction: deleteRoute,
})(Routes);
