import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import uniqid from 'uniqid';

import { addRoute } from '../../../redux/actions/routeActions';

import './index.scss';
import cities from '../../../data/cities.json';

class NewRoute extends Component {
  constructor(props) {
    super(props);

    this.state = {
      origins: [],
      destinations: [],
    };

    this.setOrigins = this.setOrigins.bind(this);
    this.setDestinations = this.setDestinations.bind(this);
    this.newRoute = this.newRoute.bind(this);
  }

  componentDidMount() {
    this.setOrigins();
    this.setDestinations();
  }

  setOrigins() {
    this.setState({ origins: cities });
  }

  setDestinations() {
    this.setState({ destinations: cities });
  }

  newRoute(event) {
    event.preventDefault();

    const { addRouteAction } = this.props;
    const formData = new FormData(this.form);

    const jsonObject = { id: uniqid() };
    formData.forEach((value, key) => { jsonObject[key] = value; });

    addRouteAction(jsonObject);
  }

  render() {
    const { origins, destinations } = this.state;

    return (
      <section className="new-route">
        <header>Nueva ruta</header>
        <form onSubmit={this.newRoute} ref={(form) => { this.form = form; }}>
          <input id="new-route-company" name="company_name" placeholder="Compañía" required />

          <select id="new-route-from" name="origin_id" required>
            <option value="">De</option>
            { origins.map(
              origin => <option value={origin.id} key={origin.id}>{origin.name}</option>,
            ) }
          </select>

          <select id="new-route-to" name="destination_id" required>
            <option value="">A</option>
            { destinations.map(
              destination => (
                <option value={destination.id} key={destination.id}>{destination.name}</option>
              ),
            ) }
          </select>

          <input type="date" name="departure_time" placeholder="Salida" required />

          <input type="date" name="arrival_time" placeholder="Llegada" required />

          <button type="submit">Agregar</button>
        </form>
      </section>
    );
  }
}

NewRoute.propTypes = {
  addRouteAction: PropTypes.func.isRequired,
};

export default connect(null, {
  addRouteAction: addRoute,
})(NewRoute);
