require('dotenv').config();
const path = require('path');
const { HotModuleReplacementPlugin, DefinePlugin } = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const definePlugin = new DefinePlugin({
  'process.env': {
    API_HOST: JSON.stringify(process.env.API_HOST || 'https://0sysjslkra.execute-api.us-east-1.amazonaws.com'),
    API_KEY: JSON.stringify(process.env.API_KEY || ''),
  },
});

const plugins = [
  new HtmlWebpackPlugin({
    title: 'Frontend boilerplate',
    template: './client/index.html',
  }),
  new HotModuleReplacementPlugin(),
  definePlugin,
];

module.exports = {
  mode: 'development',
  devtool: 'inline-source-map',
  watch: true,
  entry: {
    main: './client/index.jsx',
  },
  output: {
    path: path.resolve('./public'),
    filename: '[name].js',
    publicPath: 'http://localhost:9090/',
  },
  module: {
    rules: [{
      test: /\.jsx?$/,
      exclude: {
        test: path.resolve(__dirname, 'node_modules'),
      },
      loader: 'babel-loader',
    }, {
      test: /\.s?css$/,
      use: [
        'style-loader',
        'css-loader',
        'sass-loader',
      ],
    }],
  },
  resolve: {
    modules: [
      'node_modules',
      path.resolve('./client'),
      path.resolve('./'),
    ],
    extensions: ['.jsx', '.js', '.css', '.scss'],
  },
  plugins,
  devServer: {
    port: 9090,
    hot: true,
    headers: { 'Access-Control-Allow-Origin': '*' },
    historyApiFallback: true,
  },
};
